﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace goncharenko
{
    class Program
    {
        private struct Interval
        {
            public int start;
            public int end;
            public int count;
        }

        static int[] qs = new[] { 10, 5, 1 };
        static int[] ps = new[] { 10, 5, 1 };
        static int[] cs = new[] { 90, 40, 10 };
               
        static void Main(string[] args)
        {
            Console.WriteLine("Файл  для парсинга в Excel лежит в *папка с проектом\\bin\\Debug\\netcoreapp3.1\\file.txt* ");
            
            var variants = GetVariants().ToList();
            var statistics = GetStatistics(variants).ToList();
            ShowStatistics(statistics);

            Console.WriteLine("K = {0}", GetK(variants));
            WriteToFile(statistics);
        }

        static IEnumerable<int> GetVariants()
        {
            foreach (var q in qs)
            {
                foreach (var p in ps)
                {
                    foreach (var c in cs)
                    {
                        yield return q * p - c;
                    }
                }
            }
        }

        static IEnumerable<Interval> GetStatistics(IEnumerable<int> variants)
        {
            for (int i = -100; i <= 80; i += 20)
            {
                var count = variants.Count(v => (v >= i && v <= i + 20));
                
                yield return new Interval
                {
                    start = i,
                    count = count,
                    end = i + 20
                };
            }
        }

        static double GetK(IEnumerable<int> variants)
        {
            var positive = variants.Where(v => v > 0).ToArray();
            var negative = variants.Where(v => v < 0).ToArray();
            
            var k = (double)positive.Sum() * positive.Length / (double)negative.Sum() * negative.Length;
            return k;
        }

        static void ShowStatistics(IEnumerable<Interval> statistics)
        {
            foreach (var interval in statistics)
            {
                Console.WriteLine("[{0};{1}]   Count: {2}", interval.start, interval.end, interval.count);
            }
        }

        static void WriteToFile(IEnumerable<Interval> statistics)
        {
            var file = File.CreateText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "file.txt"));

            foreach (var interval in statistics)
            {
                file.WriteLine("{0};{1};{2}", interval.start, interval.end, interval.count);
            }
            file.Close();
        }
    }
}
